package com.romychab.test.socials

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.romychab.test.R
import com.romychab.test.utils.SocialInitializationException
import com.romychab.test.utils.SocialLoginFailedException
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import io.reactivex.Single
import io.reactivex.subjects.SingleSubject

/**
 * Login into the app via Twitter.
 */
class TwitterLogin(
    private val debugMode: Boolean = false
) : LifecycleAwareSocialLogin {

    private var activity: Activity? = null

    private var twitterAuthClient: TwitterAuthClient? = null

    private var subject: SingleSubject<SocialData>? = null

    override fun onCreate(activity: Activity) {
        this.activity = activity
        init(activity)
        this.twitterAuthClient = TwitterAuthClient()
    }

    override fun onDestroy() {
        this.activity = null
        this.twitterAuthClient?.cancelAuthorize()
        this.twitterAuthClient = null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        twitterAuthClient?.onActivityResult(requestCode, resultCode, data)
    }

    private fun init(activity: Activity) {
        val config = TwitterConfig.Builder(activity.applicationContext)
            .logger(DefaultLogger(Log.DEBUG))
            .twitterAuthConfig(TwitterAuthConfig(
                activity.getString(R.string.com_twitter_sdk_android_CONSUMER_KEY),
                activity.getString(R.string.com_twitter_sdk_android_CONSUMER_SECRET)
            ))
            .debug(this.debugMode)
            .build()
        Twitter.initialize(config)
    }

    override fun startLogin(): Single<SocialData> {
        if (null == twitterAuthClient || null == activity) {
            return Single.error(SocialInitializationException())
        }
        subject = SingleSubject.create()
        twitterAuthClient?.authorize(activity, object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {
                val session = result.data
                if (null == session) {
                    notifyError()
                    return
                }
                val authToken = session.authToken
                if (null == authToken) {
                    notifyError()
                    return
                }
                notifySuccess(SocialData(Provider.TWITTER, authToken.token, authToken.secret))
            }

            override fun failure(e: TwitterException) {
                notifyError()
            }
        })
        return subject!!
    }

    private fun notifySuccess(socialData: SocialData) {
        subject?.onSuccess(socialData)
        subject = null
    }

    private fun notifyError() {
        subject?.onError(SocialLoginFailedException(Provider.TWITTER.toString()))
        subject = null
    }

}