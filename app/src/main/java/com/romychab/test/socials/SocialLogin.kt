package com.romychab.test.socials

import com.romychab.test.utils.SocialInitializationException
import com.romychab.test.utils.SocialLoginFailedException
import io.reactivex.Single

interface SocialLogin {

    /**
     * @throws SocialLoginFailedException
     * @throws SocialInitializationException
     */
    fun startLogin(): Single<SocialData>

}