package com.romychab.test.socials

class SocialData(
    val provider: Provider,
    val token: String,
    val secret: String = ""
)

enum class Provider(private val value: String) {
    FACEBOOK("facebook"),
    TWITTER("twitter");

    override fun toString() = value
}