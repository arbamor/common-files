package com.romychab.test.socials

import android.app.Activity
import android.content.Intent
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.romychab.test.utils.SocialInitializationException
import com.romychab.test.utils.SocialLoginFailedException
import io.reactivex.Single
import io.reactivex.subjects.SingleSubject
import java.util.*

/**
 * Login via facebook.
 * Login activity must call lifecycle methods: [onActivityResult], [onCreate] and [onDestroy].
 * Method [startLogin] should be called only after [onCreate] and before [onDestroy].
 */
class FacebookLogin : LifecycleAwareSocialLogin {

    private var callbackManager: CallbackManager? = null
    private var activity: Activity? = null

    private var subject: SingleSubject<SocialData>? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreate(activity: Activity) {
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().registerCallback(callbackManager, callback)
        this.activity = activity
    }

    override fun onDestroy() {
        if (null != callbackManager) {
            LoginManager.getInstance().unregisterCallback(callbackManager)
        }
        this.activity = null
        this.callbackManager = null
    }

    override fun startLogin(): Single<SocialData> {
        if (null == callbackManager || null == activity) {
            return Single.error(SocialInitializationException())
        }

        subject = SingleSubject.create()
        LoginManager.getInstance().logInWithReadPermissions(
            activity,
            Arrays.asList("email", "public_profile")
        )
        return subject!!
    }

    private fun notifySuccess(socialData: SocialData) {
        subject?.onSuccess(socialData)
        subject = null
    }

    private fun notifyError() {
        subject?.onError(SocialLoginFailedException(Provider.FACEBOOK.toString()))
        subject = null
    }

    private fun notifyCancelled() {
        subject?.onError(SocialLoginFailedException(Provider.FACEBOOK.toString()))
        subject = null
    }

    private val callback = object : FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
            try {
                if (null == result) {
                    notifyError()
                    return
                }
                val accessToken = result.accessToken
                if (null == accessToken) {
                    notifyError()
                    return
                }
                notifySuccess(SocialData(Provider.FACEBOOK, accessToken.token))
            } catch (e: Exception) {
                e.printStackTrace()
                notifyError()
            }
        }

        override fun onCancel() {
            notifyCancelled()
        }

        override fun onError(error: FacebookException?) {
            notifyError()
        }
    }
}