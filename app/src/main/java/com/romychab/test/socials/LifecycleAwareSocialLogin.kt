package com.romychab.test.socials

import android.app.Activity
import android.content.Intent

/**
 * If the implementation of logging-in via social networks requires activity dependency
 * (e.g. showing web-view in other activity or launching another app), it should extend
 * this interface instead of [SocialLogin].
 * In this case [startLogin] has to be called after [onCreate] and before [onDestroy].
 */
interface LifecycleAwareSocialLogin : SocialLogin {

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)

    fun onCreate(activity: Activity)

    fun onDestroy()

}