package com.romychab.test

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

/**
 * App does nothing.
 * This project contains a set of files that can be reused in other project.
 * - common adapters (package: adapters)
 * - image loader (base interface and one implementation based on Glide library - see 'imageloader' package)
 * - helper classes for pagination based on Paging library from Android Arch. Components (see 'paging' package)
 * - classes to log-in users via Twitter and Facebook (see 'socials' package)
 * - some minor utils classes in package 'utils'
 */

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
