package com.romychab.test.imageloader

import android.widget.ImageView

/**
 * Base interface for image loaders.
 */
interface ImageLoader {

    /**
     * Loads image by its URL into the specified [ImageView]
     */
    fun loadImage(url: String, into: ImageView, placeholderRes: Int = 0)

}