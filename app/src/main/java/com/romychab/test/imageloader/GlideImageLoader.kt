package com.romychab.test.imageloader

import android.app.Activity
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.databinding.BindingAdapter
import android.os.Handler
import android.os.Looper
import android.support.annotation.DrawableRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy.SOURCE
import com.romychab.test.utils.isTrimEmpty
import com.romychab.test.utils.threading.PostCalls
import java.lang.ref.WeakReference

/**
 * [ImageLoader] that uses Glide library to download and show images.
 * All load requests are deferred until the size of views are calculated by the system,
 * so Glide can use memory more efficiently by reducing image size.
 */
class GlideImageLoader private constructor(
    private val glideHolder: GlideHolder
) : ImageLoader, LifecycleObserver {

    constructor(fragment: Fragment) : this(GlideHolder(fragment = fragment))
    constructor(activity: FragmentActivity) : this(GlideHolder(activity = activity))

    private val handler = Handler(Looper.getMainLooper())
    private val postCalls = PostCalls<GlideHolder>()

    override fun loadImage(url: String, into: ImageView, @DrawableRes placeholderRes: Int) {
        this.postCalls {
            it.glide()
                .load(url)
                .diskCacheStrategy(SOURCE)
                .also {
                    if (placeholderRes != 0)
                        it.placeholder(placeholderRes)
                }
                .into(into)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun resume() {
        this.handler.post { // wait for imageview's width and height
            this.postCalls.activate(this.glideHolder)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun pause() {
        this.handler.post {
            this.postCalls.deactivate()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        this.postCalls.destroy()
    }

    companion object {

        @BindingAdapter(value = ["imageUrl", "imageLoader"])
        @JvmStatic fun loadImage(imageView: ImageView, imageUrl: String?, loader: ImageLoader) {
            if (null == imageUrl || imageUrl.isTrimEmpty()) {
                imageView.setImageResource(android.R.color.transparent)
                return
            }
            loader.loadImage(imageUrl, imageView)
        }

        const val TAG = "GlideImageLoader"
    }

    class GlideHolder(
        activity: FragmentActivity? = null,
        fragment: Fragment? = null
    ) {

        private val activityRef = WeakReference<Activity>(activity)
        private val fragmentRef = WeakReference<Fragment>(fragment)

        fun glide(): RequestManager {
            fragmentRef.get()?.let { return Glide.with(it) }
            activityRef.get()?.let { return Glide.with(it) }
            throw Exception("Invalid state: no activity/fragment!")
        }
    }
}