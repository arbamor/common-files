package com.romychab.test.paging.entities


enum class LoadingStatus {
    EMPTY,
    LOADING,
    ERROR,
    SUCCESS;
}