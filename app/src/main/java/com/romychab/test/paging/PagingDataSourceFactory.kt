package com.romychab.test.paging

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.romychab.test.paging.entities.LoadingStatus
import com.romychab.test.paging.entities.Page
import com.romychab.test.paging.entities.PagedData
import io.reactivex.Single

typealias PageLoaderCreator<T> = (Page) -> Single<PagedData<T>>

class PagingDataSourceFactory<T>(
    private val status: MutableLiveData<LoadingStatus>,
    private val pageLoaderCreator: PageLoaderCreator<T>
) : DataSource.Factory<Int, T>(){
    
    override fun create(): DataSource<Int, T> {
        return PagingDataSource(status, pageLoaderCreator)
    }

}