package com.romychab.test.paging.entities

import com.romychab.test.utils.Cacheable


/**
 * Represents the piece of some large data source
 */
class PagedData<T>(
    val list: List<T>,
    val total: Int, // total items in the origin data source
    val offset: Int // position in the origin data source from where data in [list] has been read
) : Cacheable {

    override var cached: Boolean = false

}
