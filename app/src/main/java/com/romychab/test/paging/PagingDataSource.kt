package com.romychab.test.paging

import android.annotation.SuppressLint
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PositionalDataSource
import android.util.Log
import com.romychab.test.paging.entities.LoadingStatus
import com.romychab.test.paging.entities.Page
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Custom [PositionalDataSource]
 * This class works in pair with [PagingDataSourceFactory]
 */
class PagingDataSource<T>(
    private val status: MutableLiveData<LoadingStatus>,
    private val pageLoaderCreator: PageLoaderCreator<T>
) : PositionalDataSource<T>() {

    private var totalCount = -1

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<T>) {
        Log.d(TAG, "loadRange, startPos=${params.startPosition}, loadSize=${params.loadSize}")

        if (this.totalCount != -1 && params.startPosition >= this.totalCount) {
            callback.onResult(emptyList())
            return
        }

        load(
            Page(params.startPosition, params.loadSize)
        ) { list, _, _ ->
            callback.onResult(list)
        }
    }


    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<T>) {
        Log.d(TAG, "loadInitial, start=${params.requestedStartPosition}, size=${params.requestedLoadSize}")
        val count = params.requestedLoadSize
        // offset for the initial request MUST be adjusted by hands to avoid runtime crashes!
        val preOffset = params.requestedStartPosition - params.requestedStartPosition.rem(params.pageSize) - params.pageSize
        val offset = if (preOffset < 0) 0 else preOffset

        Log.d(TAG, "loadInitial, adjusted start=$offset, adjusted count=$count")

        load(
            Page(offset, count)
        ) { list, responseOffset, responseCount ->
            this.totalCount = responseCount
            callback.onResult(list, responseOffset, responseCount)
        }
    }

    private fun load(page: Page, callback: (List<T>, Int, Int) -> Unit) {
        status.postValue(LoadingStatus.LOADING)

        try {
            val res = pageLoaderCreator.invoke(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .blockingGet()
            callback.invoke(res.list, res.offset, res.total)
            publishStatus(LoadingStatus.SUCCESS)
        } catch (e: Exception) {
            e.printStackTrace()
            publishStatus(LoadingStatus.ERROR)
        }
    }

    @SuppressLint("CheckResult")
    private fun publishStatus(status: LoadingStatus) {
        Completable.complete()
            .delay(20, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                this.status.postValue(status)
            }
    }

    companion object {
        @JvmStatic val TAG = PagingDataSource::class.java.simpleName!!
    }
}