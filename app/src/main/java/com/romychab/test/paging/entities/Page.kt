package com.romychab.test.paging.entities

/**
 * Entity used in requests that support pagination
 */
data class Page(
    val offset: Int,
    val count: Int
)