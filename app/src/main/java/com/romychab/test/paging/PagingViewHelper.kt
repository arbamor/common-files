package com.romychab.test.paging

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.view.View
import com.romychab.test.paging.entities.LoadingStatus
import java.util.concurrent.Executors

/**
 * Helper class for loading items with pagination support.
 * Contains common [LiveData] instances for views that display:
 * content, progress-bars, error message
 */
class PagingViewHelper<T>(
    private val pageSize: Int = 10,
    loader: PageLoaderCreator<T>
) {

    private val status = MutableLiveData<LoadingStatus>().also { it.value = LoadingStatus.EMPTY }

    /**
     * Visibility of large progress bar displayed when content has not been loaded yet
     */
    val mainProgressVisibility: LiveData<Int> = Transformations.map(status) {
        if (it == LoadingStatus.LOADING && data.value.isNullEmpty())
            View.VISIBLE
        else View.GONE
    }

    /**
     * Visibility for content list
     */
    val listVisibility: LiveData<Int> = Transformations.map(status) {
        if (!data.value.isNullEmpty()) View.VISIBLE else View.GONE
    }

    /**
     * Visibility for components that display error messages
     */
    val errorVisibility: LiveData<Int> = Transformations.map(status) {
        if (data.value.isNullEmpty() && it == LoadingStatus.ERROR)
            View.VISIBLE
        else View.GONE
    }

    /**
     * Visibility for components that display messages like 'No items', etc.
     */
    val emptyVisibility: LiveData<Int> = Transformations.map(status) {
        if (data.value.isNullEmpty()
            && (it == LoadingStatus.SUCCESS || it == LoadingStatus.EMPTY))
            View.VISIBLE
        else View.GONE
    }

    /**
     * Visibility for refresh button
     */
    val refreshButtonVisibility: LiveData<Int> = Transformations.map(status) {
        if (!data.value.isNullEmpty()
            && (it == LoadingStatus.SUCCESS || it == LoadingStatus.ERROR)) View.VISIBLE
        else View.INVISIBLE
    }

    /**
     * Visibility for minor progress bars (e.g. in toolbars), that don't overlap content list
     */
    val minorProgressVisibility: LiveData<Int> = Transformations.map(status) {
        if (!data.value.isNullEmpty()
            && it == LoadingStatus.LOADING) View.VISIBLE
        else View.INVISIBLE
    }

    /**
     * If there is only one progressbar and it doesn't overlap main content, then use this
     * visibility value for it
     */
    val minorAndMainProgressVisibility: LiveData<Int> = Transformations.map(status) {
        if (it == LoadingStatus.LOADING) View.VISIBLE
        else View.GONE
    }

    val data = LivePagedListBuilder(
        PagingDataSourceFactory(status, loader),
        PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(this.pageSize)
            .build()
    )
    .setFetchExecutor(Executors.newSingleThreadExecutor())
    .build()


    fun invalidate() {
        data.value?.dataSource?.invalidate()
    }

}

fun <T> List<T>?.isNullEmpty(): Boolean {
    if (null == this) return true // null = empty
    return this.isEmpty()
}