package com.romychab.test.utils

class ConnectionException : Exception()

class SocialLoginFailedException(providerName: String) : Exception("Failed to login with '$providerName'")
class SocialInitializationException() : Exception("Failed to start login flow via social networks!")