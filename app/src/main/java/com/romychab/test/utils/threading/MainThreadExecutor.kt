package com.romychab.test.utils.threading

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor

/**
 * [Executor] implementation that launches tasks on main thread
 */
class MainThreadExecutor : Executor {

    private val handler = Handler(Looper.getMainLooper())

    override fun execute(command: Runnable) {
        if (Looper.getMainLooper().thread == Thread.currentThread())
            command.run()
        else
            handler.post(command)
    }

}