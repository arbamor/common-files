package com.romychab.test.utils.threading

import android.support.annotation.MainThread

/**
 * Helper class that invokes calls called via [PostCalls.invoke] only in active state of [PostCalls]
 * instance.
 * Otherwise calls are put into queue and will be called during the next activation
 * ([activate] method call)
 */
class PostCalls<T> {

    private var obj: T? = null
    private var isDestroyed = false

    private val queue = mutableListOf<(T) -> Unit>()

    /**
     * Activate the instance and run all calls in queue one-by-one.
     * After calling this method all calls passed to [invoke] will be called immediately
     * @param obj object that will be passed to calls during invocation
     */
    @MainThread
    fun activate(obj: T) {
        if (this.isDestroyed) return
        this.obj = obj
        this.queue
            .toList() // make a copy
            .forEach { it.invoke(obj) }
        this.queue.clear()
    }

    /**
     * Deactivates the instance, so all incoming calls will be added to queue instead of calling
     */
    @MainThread
    fun deactivate() {
        if (this.isDestroyed) return
        this.obj = null
    }

    /**
     * Destroys the instance, so all future calls will be discarded
     */
    @MainThread
    fun destroy() {
        deactivate()
        if (this.isDestroyed) return
        this.isDestroyed = true
        this.queue.clear()
    }

    /**
     * Invoke call immediately in active state or postpone it by adding to call queue in inactive
     * state
     */
    operator fun invoke(call: (T) -> Unit) {
        if (this.isDestroyed) return
        this.obj
            ?.let { call.invoke(it) } // in active state - call immediately
            ?: this.queue.add(call) // otherwise - put the call to the queue
    }

    companion object {

        @JvmStatic
        private val EMPTY = PostCalls<Any>().also { it.destroy() }

        @Suppress("UNCHECKED_CAST")
        @JvmStatic
        fun <T> empty(): PostCalls<T> = EMPTY as PostCalls<T>

    }
}