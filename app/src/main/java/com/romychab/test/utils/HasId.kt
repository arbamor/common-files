package com.romychab.test.utils

/**
 * FLag interface indicates that the entity has ID
 */
interface HasId {

    val id: Long

}

private fun <T : HasId> getIdPredicate(id: Long): (T) -> Boolean {
    return { it -> it.id == id }
}

/**
 * Set of helper methods for the collections of entities marked by [HasId] interface
 */

fun <T : HasId> Collection<T>.containsById(entity: T): Boolean {
    return null != getById(entity.id)
}

fun <T : HasId> Collection<T>.containsById(id: Long): Boolean {
    return null != getById(id)
}

fun <T : HasId> MutableCollection<T>.removeById(id: Long): Boolean {
    return this.removeAll(getIdPredicate(id))
}

fun <T : HasId> Collection<T>.getById(id: Long): T? {
    return this.firstOrNull(getIdPredicate(id))
}

fun <T : HasId> Collection<T>.indexById(id: Long): Int {
    return this.indexOfFirst(getIdPredicate(id))
}

fun <T : HasId> List<T>.replace(newEntity: T): List<T> {
    return this.map { if (it.id == newEntity.id) newEntity else it }
}