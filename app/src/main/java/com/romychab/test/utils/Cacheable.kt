package com.romychab.test.utils

import io.reactivex.Observable

/**
 * Flag to determine whether the entity has been received from the actual data source or from
 * the cache.
 */
interface Cacheable {
    val cached: Boolean

    fun isActual(): Boolean = !cached
}

fun <T : Cacheable> Observable<T>.unsubscribeAfterFirstActualItem(): Observable<T> {
    return this.takeUntil { it.isActual() }
        .switchIfEmpty(Observable.error<T>(ConnectionException()))
}