package com.romychab.test.utils

fun String?.isTrimEmpty(): Boolean {
    return this?.trim()?.isEmpty() ?: true
}