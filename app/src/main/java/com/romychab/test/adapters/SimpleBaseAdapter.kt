package com.romychab.test.adapters

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.romychab.test.utils.HasId

/**
 * Easy and lightweight adapter for ListViews
 */
abstract class SimpleBaseAdapter<T : HasId, B : ViewDataBinding>(
    private val itemLayoutId: Int,
    private val list: List<T>
) : BaseAdapter() {

    @Suppress("UNCHECKED_CAST")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val binding = if (null == convertView) {
            val inflater = LayoutInflater.from(parent.context)
            val binding = DataBindingUtil.inflate<B>(inflater, this.itemLayoutId, parent, false)
            binding.root.tag = binding
            binding
        } else {
            convertView.tag as B
        }
        bindView(binding, getItem(position))
        return binding.root
    }

    override fun getItem(position: Int): T {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return list[position].id
    }

    override fun getCount(): Int {
        return list.size
    }

    protected abstract fun bindView(binding: B, item: T)
}