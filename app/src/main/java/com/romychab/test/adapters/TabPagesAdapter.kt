package com.romychab.test.adapters

import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup

class SimplePage(
    val title: String,  // will be displayed in tab
    val view: View
)

/**
 * Adapter for [ViewPager] that handles the list of pre-inflated views
 */
class TabPagesAdapter(
    private val pages: List<SimplePage>
) : PagerAdapter() {

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = pages[position].view
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun getCount(): Int {
        return this.pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return this.pages[position].title
    }

}