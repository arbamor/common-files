package com.romychab.test.adapters

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.romychab.test.utils.HasId

/**
 * Base RecyclerView adapter for lists of entities with IDs
 */
open class BaseBindAdapter<T : HasId, B : ViewDataBinding>(
    private val bindingLayoutRes: Int,
    private val binder: (item: T, binding: B) -> Unit
) : RecyclerView.Adapter<BindableHolder<B>>() {

    private var items: List<T> = emptyList()

    /**
     * Submit new list to be displayed. Difference between old list and new one
     * will be calculated automatically and the corresponding animations will be executed.
     */
    fun submitNewList(items: List<T>) {
        if (this.items.isEmpty()) {
            this.items = items
            notifyDataSetChanged()
            return
        }
        val diffCallback = DiffCallback(this.items, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.items = items
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindableHolder<B> {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<B>(inflater, bindingLayoutRes, parent, false)
        return BindableHolder(binding)
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    override fun onBindViewHolder(holder: BindableHolder<B>, position: Int) {
        val item = this.items[position]
        this.binder.invoke(item, holder.binding)
    }

    private class DiffCallback<T : HasId>(
        private val oldList: List<T>,
        private val newList: List<T>
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

        override fun getOldListSize(): Int = oldList.size
        override fun getNewListSize(): Int = newList.size

    }

}

class BindableHolder<B : ViewDataBinding>(val binding: B) : RecyclerView.ViewHolder(binding.root)